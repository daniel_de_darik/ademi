from django.conf import settings
from celery.task import Task
from django.core.mail import get_connection


BACKEND = getattr(
    settings,
    'CELERY_EMAIL_BACKEND', 'django.core.mail.backends.smtp.EmailBackend')


class SendEmailTask(Task):

    def run(self, email_messages, **kwargs):
        con = get_connection(backend=BACKEND)
        con.send_messages(email_messages)
