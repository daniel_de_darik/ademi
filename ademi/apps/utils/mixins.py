from django.http import (
    Http404,
    HttpResponseRedirect,
    HttpResponse)
import re
import simplejson

ACTION_RE = re.compile(r'\w+')


class FormRequestMixin(object):

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(FormRequestMixin, self).get_form_kwargs(*args, **kwargs)
        kwargs['request'] = self.request
        return kwargs


class ActionMixin(object):
    """Mixin for managing actions
    """

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action', None)
        self.kwargs = kwargs

        if action is None or not ACTION_RE.search(action):
            raise Http404
        response = None
        do_action = getattr(self, 'on_{}'.format(action), None)
        if do_action is not None and callable(do_action):
            response = do_action(request, action)
        else:
            raise Http404

        if response is not None:
            return response
        return HttpResponseRedirect(self.get_success_url())


class JSONResponseMixin(object):
    r"""Mixin that helps to work with json request-response processes.
    Basically overrides render_to_response. Pluggable to generic views."""

    def render_to_response(self, context, **kwargs):
        r""" Returns a JSON response containing 'context' as payload """
        return self.get_json_response(
            self.convert_context_to_json(context), **kwargs)

    def get_json_response(self, content, **kwargs):
        return HttpResponse(
            content, content_type='application/json', **kwargs)

    def convert_context_to_json(self, context):
        try:
            return simplejson.dumps(context)
        except TypeError, e:
            print "JSON encode error. {}".format(e)
