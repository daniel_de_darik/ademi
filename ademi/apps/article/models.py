from django.db import models
from datetime import datetime
from django.core.urlresolvers import reverse
import mongoengine as me
# Create your models here.


class Like(me.EmbeddedDocument):
	username = me.StringField(required=True, max_length=32)
	user_id = me.IntField(required=True)


class Article(me.Document):
	title = me.StringField(required=True, max_length=256)
	body = me.StringField(required=True)
	date_created = me.DateTimeField(default=datetime.now)
	image = me.StringField(required=True)
	username = me.StringField(required=True, max_length=32)
	user_id = me.IntField(required=True)
	likes_list = me.ListField(me.EmbeddedDocumentField(Like))
	likes_cnt = me.IntField(default=0)
	views_cnt = me.IntField(default=0)
	comments_cnt = me.IntField(default=0)

	def get_absolute_url(self):
		return reverse('article-detail', args=[self.id])

