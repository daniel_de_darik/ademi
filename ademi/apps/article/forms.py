from django import forms
from article.models import Article, Like
from django.utils.translation import ugettext as _

class ArticleForm(forms.Form):

	def __init__(self, *args, **kwargs):
		request = kwargs.pop('request', None)
		self.user = request.user
		self.instance = kwargs.pop('instance', None)
		super(ArticleForm, self).__init__(*args, **kwargs)

		if self.instance:
			self.fields['title'].initial=self.instance.title
			self.fields['body'].initial=self.instance.body
			self.fields['image'].initial=self.instance.image

	title = forms.CharField(required=True, max_length=256)
	body = forms.CharField(required=True, widget=forms.widgets.Textarea())
	image = forms.CharField(required=True)

	def clean_title(self):
		title = self.cleaned_data.get('title', '')
		if len(title) == 0:
			raise forms.ValidationError(_('Title is too short'))
		return title

	def clean_body(self):
		body = self.cleaned_data.get('body', '')
		if len(body) == 0:
			raise forms.ValidationError(_('Body is too short'))
		return body

	def clean_image(self):
		image = self.cleaned_data.get('image', '')
		if len(image) == 0:
			raise forms.ValidationError(_('No image found'))
		return image

	def save(self, commit=True):
		title = self.cleaned_data.get('title', '')
		body = self.cleaned_data.get('body', '')
		image = self.cleaned_data.get('image', '')

		if self.instance:
			article = self.instance
		else:
			article = Article(
				username=self.user.username,
				user_id=self.user.id,
				likes_list=[])
		article.title = title
		article.body = body
		article.image = image
		if commit:
			article.save()

		return article


class LikeForm(forms.Form):

	def __init__(self, *args, **kwargs):
		request = kwargs.pop('request', None)
		self.user = None
		self.article = None
		if request:
			self.user = request.user

		super(LikeForm, self).__init__(*args, **kwargs)

	article_id = forms.CharField(required=True)

	def clean_article_id(self):
		article_id = self.cleaned_data.get('article_id', 0)
		try:
			self.article = Article.objects.get(pk=article_id)
		except Article.DoesNotExist:
			raise forms.ValidationError(_('Article does not exist'))

	def save(self, commit=True):
		liked = False
		for like in self.article.likes_list:
			if like.user_id == self.user.id:
				liked = True
				break

		if not liked:
			like = Like(
				username=self.user.username,
				user_id=self.user.id)
			self.article.likes_list.append(like)
			self.article.likes_cnt += 1
		else:
			likes = [like for like in self.article.likes_list if like.user_id != self.user.id]
			self.article.likes_list = likes
			self.article.likes_cnt -= 1

		if commit:
			self.article.save()

		self.article.liked = not liked

		return self.article
