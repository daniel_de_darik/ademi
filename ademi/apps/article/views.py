from django.http import HttpResponseRedirect, Http404
from django.core.urlresolvers import reverse
from django.contrib import messages
from article.models import Article
from article.forms import ArticleForm, LikeForm
from comment.models import Comment
from django.views.generic.edit import (
	CreateView,
	FormView,
	UpdateView)
from django.views.generic.detail import DetailView
from utils.mixins import FormRequestMixin
from django.utils.translation import ugettext_lazy as _


class ArticleCreateView(FormRequestMixin, CreateView):
	model = Article
	form_class = ArticleForm
	template_name = 'create.html'

	def get_success_url(self):
		return self.object.get_absolute_url()


class ArticleDetailView(DetailView):
	model = Article
	template_name = 'detail.html'
	context_object_name = 'article'
	
	def get_context_data(self, *args, **kwargs):
		context = super(ArticleDetailView, self).get_context_data(*args, **kwargs)
		article = self.get_object()

		comments = Comment.objects.filter(article=article)
		context['comment_list'] = comments

		return context

	def get_object(self, queryset=None):
		return Article.objects.get(pk=self.kwargs.get('pk'))


class ArticleUpdateView(FormRequestMixin, UpdateView):
	model = Article
	form_class = ArticleForm
	template_name = 'create.html'
	
	def get_object(self, queryset=None):
		return Article.objects.get(pk=self.kwargs.get('pk'))

	def get_success_url(self):
		return self.object.get_absolute_url()


class LikeView(FormRequestMixin, FormView):
	form_class = LikeForm

	def form_valid(self, form):
		article = form.save()
		if article.liked:
			messages.success(
				self.request, _('You have successfully liked the article'))
		else:
			messages.success(
				self.request, _('You have successfully unliked the article'))

		return HttpResponseRedirect(reverse('article-detail', args=[article.id]))

	def form_invalid(self, form):
		messages.error(
			self.request, _('Ooopsss! What a shame! Try your request later'))
		print form.__dict__
		article_id = form.data.get('article_id', 0)
		return HttpResponseRedirect(reverse('article-detail', args=[article_id]))
