from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from article import views

urlpatterns = patterns(
    '',
    url(r'^create/$',
        login_required(
            views.ArticleCreateView.as_view()), name='article-create'),
    url(r'^like/$',
        login_required(
            views.LikeView.as_view()), name='article-like'),
    url(r'^update/(?P<pk>\w+)/$',
        login_required(
            views.ArticleUpdateView.as_view()), name='article-update'),
    url(r'^(?P<pk>\w+)/$',
        login_required(
            views.ArticleDetailView.as_view()), name='article-detail'),
)
