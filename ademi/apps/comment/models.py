from article.models import Article
import mongoengine as me
from datetime import datetime

class Comment(me.Document):
	article = me.ReferenceField(Article, reverse_delete_rule=me.CASCADE)
	username = me.StringField(required=True, max_length=32)
	user_id = me.IntField(required=True)
	body = me.StringField(required=True)
	date_created = me.DateTimeField(default=datetime.now)
