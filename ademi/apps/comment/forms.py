from django import forms
from comment.models import Comment
from article.models import Article
from django.utils.translation import ugettext as _

class CommentForm(forms.Form):

	def __init__(self, *args, **kwargs):
		request = kwargs.pop('request', None)
		if request:
			self.user = request.user
		self.article = None
		kwargs.pop('instance', None)
		super(CommentForm, self).__init__(*args, **kwargs)

	body = forms.CharField(required=True)
	article_id = forms.CharField(required=True)

	def clean_article_id(self):
		article_id = self.cleaned_data.get('article_id', 0)
		try:
			self.article = Article.objects.get(pk=article_id)
		except Article.DoesNotExist:
			raise ValidationError(_('Article does not exist'))

	def clean_body(self):
		body = self.cleaned_data.get('body', '')
		if len(body) == 0:
			raise ValidationError(_('Body is too short'))
		return body

	def save(self, commit=True):
		body = self.cleaned_data.get('body', '')
		comment = Comment(
			article=self.article,
			username=self.user.username,
			user_id=self.user.id,
			body=body)

		if commit:
			comment.save()
			self.article.comments_cnt += 1
			self.article.save()

		return comment
