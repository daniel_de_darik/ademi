from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from comment import views

urlpatterns = patterns(
    '',
    url(r'^add/$',
        login_required(views.CommentCreateView.as_view()), name='comment-add'),
)
