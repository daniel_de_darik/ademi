from django.contrib import messages
from django.http import HttpResponseRedirect
from comment.forms import CommentForm
from django.core.urlresolvers import reverse
from utils.mixins import FormRequestMixin, JSONResponseMixin
from django.views.generic import FormView
from django.utils.translation import ugettext_lazy as _


class CommentCreateView(JSONResponseMixin, FormRequestMixin, FormView):
	form_class = CommentForm

	def form_valid(self, form):
		comment = form.save()
		messages.success(
			self.request, _('Comment has been successfully added'))
		# return self.render_to_response({'status': True})
		article_id = form.data.get('article_id', 0)
		return HttpResponseRedirect(reverse('article-detail', args=[article_id]))

	def form_invalid(self, form):
		# return self.render_to_response({'status': False})
		messages.error(
			self.request, _('Body is too short'))
		article_id = form.data.get('article_id', 0)
		return HttpResponseRedirect(reverse('article-detail', args=[article_id]))
