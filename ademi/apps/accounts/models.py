from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.utils.translation import ugettext as _

GENDER_CHOICES = (
    (0, _('Male')),
    (1, _('Female')))


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    birth_date = models.DateField(null=True)
    gender = models.IntegerField(choices=GENDER_CHOICES, default=0)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)
