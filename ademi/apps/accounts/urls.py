from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from accounts.forms import MyLoginForm
from allauth.account.views import LoginView
from accounts.views import (
    ProfileUpdateView,
    ProfileDetailView)

urlpatterns = patterns(
    '',
    url(r'^login/$', LoginView.as_view(
        form_class=MyLoginForm), name="account_login"),
    url(r'^profile/$', login_required(
        ProfileDetailView.as_view()), name="profile"),
    url(r'^profile/(?P<pk>\d+)/$',
        ProfileDetailView.as_view(), name="account_profile"),
    url(r'^profile/edit/$', login_required(
        ProfileUpdateView.as_view()), name="profile_edit"),
    url(r'^', include('allauth.urls')),
    url(r'^avatar/', include('avatar.urls')),
)
