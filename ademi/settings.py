"""
Django settings for ademi project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

import os
import sys
from configurations import Configuration


BASE_DIR = os.path.dirname(os.path.dirname(__file__))


def rel(*x):
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)

sys.path.insert(0, rel('apps'))

import djcelery
djcelery.setup_loader()


class BaseConfiguration(Configuration):
    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = 'f_p!w-(2)t_m8+=tlyr)h%jw+=7a0+lv@7laurop4xn@p8#l%3'

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = True

    TEMPLATE_DEBUG = True

    ALLOWED_HOSTS = []

    ADMINS = (
        ('Darkhan Imangaliyev', 'darkhan.imangaliev@gmail.com'),
        )

    # Application definition

    INSTALLED_APPS = (
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',
        'djcelery',
        'accounts',
        'article',
        'comment',
        'celery_email',
        'allauth',
        'allauth.account',
        'allauth.socialaccount',
        'avatar',
        'rest_framework',
        'django_js_reverse',
    )

    MIDDLEWARE_CLASSES = (
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )

    TEMPLATE_CONTEXT_PROCESSORS = (
        "django.contrib.auth.context_processors.auth",
        "django.core.context_processors.debug",
        "django.core.context_processors.i18n",
        "django.core.context_processors.media",
        "django.core.context_processors.static",
        "django.core.context_processors.tz",
        "django.contrib.messages.context_processors.messages",
        "django.core.context_processors.request",
        # allauth specific context processors
        "allauth.account.context_processors.account",
        "allauth.socialaccount.context_processors.socialaccount",
    )

    ROOT_URLCONF = 'ademi.urls'

    WSGI_APPLICATION = 'ademi.wsgi.application'

    # Database
    # https://docs.djangoproject.com/en/1.7/ref/settings/#databases

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'ademi_db',
            'HOST': 'localhost',
            'USER': 'postgres',
            'PASSWORD': '111'
        }
    }

    # Memcached
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
        }
    }

    SESSION_ENGINE = "django.contrib.sessions.backends.cache"

    TEST_RUNNER = 'django.test.runner.DiscoverRunner'

    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

    # NoSql database mongodb

    MONGO_USERNAME = 'ademi'
    MONGO_PASSWORD = '111'
    MONGO_HOST = 'localhost'
    MONGO_PORT = 27017
    MONGO_DB = 'ademi_db'

    from mongoengine import connect

    connect(
        MONGO_DB, username=MONGO_USERNAME, password=MONGO_PASSWORD,
        host=MONGO_HOST, port=MONGO_PORT
        )

    # Internationalization
    # https://docs.djangoproject.com/en/1.7/topics/i18n/

    LANGUAGE_CODE = 'en-us'

    TIME_ZONE = 'UTC'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True

    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/1.7/howto/static-files/

    STATIC_URL = '/static/'
    STATIC_ROOT = 'ademi/public'
    MEDIA_ROOT = 'ademi/media'
    MEDIA_URL = '/media/'

    # User profile module
    AUTH_PROFILE_MODULE = 'accounts.models.UserProfile'

    # Global templates directory
    TEMPLATE_DIRS = (
        rel('templates'),)

    STATICFILES_DIRS = (
        rel('static'),)

    SITE_ID = 1
    SITE_DOMAIN = 'localhost:8000'

    ACCOUNT_AUTHENTICATION_METHOD = 'username_email'
    ACCOUNT_EMAIL_REQUIRED = True
    ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
    ACCOUNT_LOGOUT_ON_GET = True

    EMAIL_BACKEND = 'celery_email.CeleryEmailBackend'
    EMAIL_HOST_USER = 'servicestargame@gmail.com'
    EMAIL_HOST_PASSWORD = ''
    EMAIL_PORT = 587
    EMAIL_SUBJECT_PREFIX = '[Ademi] '
    SERVER_EMAIL = 'servicestargame@gmail.com'
    DEFAULT_FROM_EMAIL = SERVER_EMAIL
    # Django SES Amazon settings
    CELERY_EMAIL_BACKEND = 'django_ses.SESBackend'
    AWS_ACCESS_KEY_ID = 'AKIAIABNLVGLWDHLENBA'
    AWS_SECRET_ACCESS_KEY = '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'

    BROKER_URL = 'amqp://guest@localhost:5672//'

    REST_FRAMEWORK = {
        # Use Django's standard `django.contrib.auth` permissions,
        # or allow read-only access for unauthenticated users.
        'DEFAULT_PERMISSION_CLASSES': [
            'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
        ]
    }

    from django.contrib.messages import constants as messages
    MESSAGE_TAGS = {
        messages.INFO: 'list-group-item-info',
        messages.DEBUG: 'list-group-item-info',
        messages.SUCCESS: 'list-group-item-success',
        messages.WARNING: 'list-group-item-warning',
        messages.ERROR: 'list-group-item-danger',
    }


class Development(BaseConfiguration):
    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = True


class Production(BaseConfiguration):
        # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = False
