from django.views.generic.base import TemplateView

class HomePageView(TemplateView):

    template_name = 'home.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomePageView, self).get_context_data(*args, **kwargs)
        context['greet'] = "Hello, World :)"
        return context
