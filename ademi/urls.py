from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from views import HomePageView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', HomePageView.as_view(), name='home_page'),
	url(r'^article/', include('article.urls')),
	url(r'^comment/', include('comment.urls')),
	url(r'^accounts/', include('accounts.urls')),
	url(r'^jsreverse/$', 'django_js_reverse.views.urls_js', name='js_reverse'),
	url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
